/**LIBRARY::PhysicianList**/
{fnSrvPhys(NAMELOGIN)}
/* update EFTRACK22 obs term so we can tell who uses this form */
{!OBSNOW("EFTRACK22","Biologics Administration")}

{!ADD_TEXT_COMP('Enterprise', 'MedAdminTextTranslation')}
{!Document.MedRequestTranslation
  Document.MedAdministrationTranslation
  Document.MedAdministrationTranslation2
}


/********************************************************************/
/* Watcher to set visibility flag for insurance verification section
/********************************************************************/
{!DOCUMENT.VISIBILITY_FLAG=fnBiologicsAdminSetVisibilityFlag(DOCUMENT.INSURANCE_RB1,OBSANY("BIOLOGICNAME"),OBSANY("CINQAIR BNB"),OBSANY("DUPIXENT BNB"),OBSANY("FASENRA BNB"),OBSANY("ILARIS BNB"),OBSANY("NUCALA BNB"),OBSANY("XOLAIR BNB"))}

!fn fnBiologicsAdminSetVisibilityFlag(insuranceRB,biologic,cinqairBnB,dupxentBnB,fasenraBnB,ilarisBnB,nucalaBnB,xolairBnB)
{
	if 	(biologic=="Cinqair" and match(cinqairBnB,"Yes")>0) or 
		(biologic=="Dupixent" and match(dupxentBnB,"Yes")>0) or 
		(biologic=="Fasenra" and match(fasenraBnB,"Yes")>0) or 
		(biologic=="Ilaris" and match(ilarisBnB,"Yes")>0) or 
		(biologic=="Nucala" and match(nucalaBnB,"Yes")>0) or 
		(biologic=="Xolair" and match(xolairBnB,"Yes")>0) then
		if ok(insuranceRB) and insuranceRB<>"" then
			return "2" //color=black
		else
			return "1" //color=red
		endif
	else
		return "" //no visibility
	endif
}
/* Initialization - runs once when form loads */
{!
	if (DOCUMENT.FIRST_TIME_THRU_FLAG=="") then
		DOCUMENT.FIRST_TIME_THRU_FLAG="1"
		DOCUMENT.RED_OR_YELLOW=""
		DOCUMENT.MSG_ALERT=fnBiologicsAdministrationGetLastOfficeVisit()
		/* Touch variables Centricity can't seem to find otherwise */
		DOCUMENT.MILLIGRAMS_SELECTED=0
		DOCUMENT.MILLIGRAMS_GIVEN=0
		DOCUMENT.LAST_DOSAGE="" 
		DOCUMENT.XOLAIR1DOSE=""
		DOCUMENT.XOLAIR2DOSE=""
		DOCUMENT.XOLAIR3DOSE=""
		DOCUMENT.XOLAIR4DOSE=""
		DOCUMENT.XOLAIR5DOSE=""
		DOCUMENT.XOLAIR6DOSE=""
		DOCUMENT.XOLAIR1AMTVOL=""
		DOCUMENT.XOLAIR2AMTVOL=""
		DOCUMENT.XOLAIR3AMTVOL=""
		DOCUMENT.XOLAIR4AMTVOL=""
		DOCUMENT.XOLAIR5AMTVOL=""
		DOCUMENT.XOLAIR6AMTVOL=""
		DOCUMENT.XOLAIRWASTED=""
		DOCUMENT.NUCALA1DOSE=""
		DOCUMENT.NUCALA2DOSE=""
		DOCUMENT.NUCALA3DOSE=""
		/* Initialize Xolair tab */
		fnBiologicsAdminXolairInit()
	endif
}
/*****************************************************/
/* Function: fnBiologicsAdminXolairInit()
/* Purpose: Called when form is first loaded to
/*			pre-fill Xolair fields if given previously.
/*****************************************************/
!fn fnBiologicsAdminXolairInit()
{
	/* Special logic to transfer old BIOLOGPAEXP observations to new BIOLOGCOMMNT observation */
	if LAST_SIGNED_OBS_DATE("BIOLOGPAEXP")<>"" 
		and durationdays(LAST_SIGNED_OBS_DATE("BIOLOGPAEXP"),"11/01/2020")>0 
		and LAST_SIGNED_OBS_DATE("BIOLOGCOMMNT")=="" then
			OBSNOW("BIOLOGCOMMNT",LAST_SIGNED_OBS_VALUE("BIOLOGPAEXP"))
			OBSNOW("BIOLOGPAEXP",".")
	endif
	if (LAST_SIGNED_OBS_VALUE("XOLAIR INJ")<>"") then
		fnBiologicsAdminXolairCalcAmt(LAST_SIGNED_OBS_VALUE("XOLAIR INJ"))
	endif
}
/*****************************************************/
/* Watcher to update BMI from height and weight 
/*****************************************************/
{!OBSNOW("BMI",str(fnBMI(OBSANY("HEIGHT"),OBSNOW("WEIGHT"))))}

!fn fnBMI()
{
   if (OBSNOW("WEIGHT")=="" OR OBSANY("HEIGHT")=="" OR OBSANY("HEIGHT")<= 0) THEN
      return ""
   endif
   return (OBSNOW("WEIGHT")/2.2)/((OBSANY("HEIGHT")/39.4)^2)
}
/***********************************************************/
/* Watchers to set drug radiobutton from Site observations
/***********************************************************/
{if (OBSNOW("XOLAIR SITE")<>"") then DOCUMENT.SELECT_DRUG="XOLAIR" endif}
{if (OBSNOW("CINQAIR SIT")<>"") then DOCUMENT.SELECT_DRUG="CINQAIR" endif}
{if (OBSNOW("DUPIXENT STE")<>"") then DOCUMENT.SELECT_DRUG="DUPIXENT" endif}
{if (OBSNOW("FASENRA SITE")<>"") then DOCUMENT.SELECT_DRUG="FASENRA" endif}
{if (OBSNOW("ILARIS SITE")<>"") then DOCUMENT.SELECT_DRUG="ILARIS" endif}
{if (OBSNOW("NUCALA SITE")<>"") then DOCUMENT.SELECT_DRUG="NUCALA" endif}

/*****************************************************/
/* Watcher to translate Vital Signs 
/*****************************************************/
{DOCUMENT.VITALS_TRANSLATION=fnBiologicsAdminTranslateVitals(OBSANY("HEIGHT"),OBSNOW("WEIGHT"),OBSNOW("WEIGHT (KG)"),OBSNOW("BMI"),OBSNOW("BP SYSTOLIC"),OBSNOW("BP DIASTOLIC"),OBSNOW("PULSE RATE"),OBSNOW("RESP RATE"),OBSNOW("02SAT"),OBSNOW("PEF"))}
/*******************************************************************/
/* Function: fnBiologicsAdminTranslateVitals()
/* Purpose:  Called from watcher above to translate vital signs into
/*			a document variable that will then translate beneath
/*			the active administration drug.
/*******************************************************************/
fn fnBiologicsAdminTranslateVitals(ht,wt,wtKg,bmi,bpSys,bpDia,pulse,resp,o2sat,PEF)
{
	local retStr=""
	if (str(ht,wt,wtKg,bmi,bpSys,bpDia,pulse,resp,o2sat,PEF)<>"") then
		retStr=retStr+HRET+FMT("Vital Signs ", "B,2")+HRET
		if (str(ht,wt,bmi)<>"") then
			retStr=retStr+CFMT(ht, "", "Height: ", "B"," in.   ")
			retStr=retStr+CFMT(wt, "", "Weight: ", "B"," lbs ("+wtKg+" kg)   ")
			retStr=retStr+CFMT(bmi, "", "BMI: ", "B","")
			retStr=retStr+HRET
		endif
		if (str(bpSys,bpDia,pulse,resp,o2sat,PEF)<>"") then
			retStr=retStr+CFMT(bpSys, "", "BP:  ", "B", "") + CFMT(bpDia, "", "/ ", "B", "   " )
			retStr=retStr+CFMT(pulse, "", "Pulse:  ", "B", "   ")
			retStr=retStr+CFMT(resp, "", "Resp:  ", "B", "   ")
			retStr=retStr+CFMT(o2sat, "", "O2 Sats:  ", "B", "   ")
			retStr=retStr+CFMT(PEF, "", "PEF:  ", "B", "")
			retStr=retStr+HRET
		endif
	endif
	return retStr
}
/*****************************************/
/*****************************************/
/* X O L A I R   S E C T I O N
/*****************************************/
/*****************************************/

/**************************************************/
/* Watcher to detect when pre-filled syringe checkbox is changed
/**************************************************/
{if ok(DOCUMENT.XOLAIR_PREFILLED_SYRINGES)then fnBiologicsAdminXolairPreFilledCheckbox(DOCUMENT.XOLAIR_PREFILLED_SYRINGES,DOCUMENT.XOLAIR_USE_ONLY_75MG) endif}

/**************************************************************/
/* Function: fnBiologicsAdminXolairPreFilledCheckbox()
/* Purpose: Called from watcher above whenever user checks or unchecks
/*			the "using pre-filled syringes" checkbox.
/**************************************************************/
fn fnBiologicsAdminXolairPreFilledCheckbox()
{
	DOCUMENT.LAST_DOSAGE="*" 
	fnBiologicsAdminXolairCalcAmt(OBSANY("XOLAIR INJ"))
}

/**************************************************/
/* Watcher to calculate Xolair amounts from selected dosage
/**************************************************/
{fnBiologicsAdminXolairCalcAmt(OBSANY("XOLAIR INJ"))}

/**************************************************************/
/* Function: fnBiologicsAdminXolairCalcAmt(dosage)
/* Purpose: Called from watcher above whenever user selects a Xolair dosage
/*			 from the DOSAGE dropdown. Note that the dosage value must be
/*			 different from the last time we were called in order to enter the
/*			 function. This fixes a bug where this function was executed
/*			 every time the visit was put on hold and then re-opened.
/**************************************************************/
!fn fnBiologicsAdminXolairCalcAmt(dosage)
{
	/* If user cleared dosage, just clear all dosing obs and return */
	if (ok(dosage) and dosage=="") then
		DOCUMENT.XOLAIR1DOSE=""
		DOCUMENT.XOLAIR2DOSE=""
		DOCUMENT.XOLAIR3DOSE=""
		DOCUMENT.XOLAIR4DOSE=""
		DOCUMENT.XOLAIR5DOSE=""
		DOCUMENT.XOLAIR6DOSE=""
		DOCUMENT.XOLAIRWASTED=""
		DOCUMENT.XOLAIR1AMTVOL=""
		DOCUMENT.XOLAIR2AMTVOL=""
		DOCUMENT.XOLAIR3AMTVOL=""
		DOCUMENT.XOLAIR4AMTVOL=""
		DOCUMENT.XOLAIR5AMTVOL=""
		DOCUMENT.XOLAIR6AMTVOL=""		
		return ""
	endif
	/* If user entered a dosage, calculate amounts */
	if (ok(dosage) and dosage<>DOCUMENT.LAST_DOSAGE) then
		DOCUMENT.LAST_DOSAGE=dosage
		fnBiologicsAdminXolairClearRows(1,2,3,4,5,6)
		/*-------------------------------------------*/
		/* Extract milligrams from selected dosage:
		/*-------------------------------------------*/
		local retVal="",inj1="",inj2="",inj3="",inj4="",inj5="",inj6=""
		cond
			case match(dosage,"150")>0
				cond
					case DOCUMENT.XOLAIR_PREFILLED_SYRINGES=="" and DOCUMENT.XOLAIR_USE_ONLY_75MG==""
						inj1="1.2 mL"
						DOCUMENT.XOLAIR1AMTVOL="150mg/1.2mL"
					case DOCUMENT.XOLAIR_PREFILLED_SYRINGES=="" and DOCUMENT.XOLAIR_USE_ONLY_75MG<>""
						inj1="0.6 mL"
						inj2="0.6 mL"
						DOCUMENT.XOLAIR1AMTVOL="75mg/0.6mL"
						DOCUMENT.XOLAIR2AMTVOL="75mg/0.6mL"
					case DOCUMENT.XOLAIR_PREFILLED_SYRINGES<>"" and DOCUMENT.XOLAIR_USE_ONLY_75MG==""
						inj1="1 mL (pre-filled syringe)"
						DOCUMENT.XOLAIR1AMTVOL="150mg/1mL"
					case DOCUMENT.XOLAIR_PREFILLED_SYRINGES<>"" and DOCUMENT.XOLAIR_USE_ONLY_75MG<>""
						inj1="0.5 mL (pre-filled syringe)"
						inj2="0.5 mL (pre-filled syringe)"
						DOCUMENT.XOLAIR1AMTVOL="75mg/0.5mL"
						DOCUMENT.XOLAIR2AMTVOL="75mg/0.5mL"
				endcond
				DOCUMENT.MILLIGRAMS_SELECTED=150
			case match(dosage,"225")>0
				cond
					case DOCUMENT.XOLAIR_PREFILLED_SYRINGES=="" and DOCUMENT.XOLAIR_USE_ONLY_75MG==""
						inj1="0.9 mL"
						inj2="0.9 mL"
						DOCUMENT.XOLAIR1AMTVOL="150mg/1.2mL"
						DOCUMENT.XOLAIR2AMTVOL="150mg/1.2mL"
					case DOCUMENT.XOLAIR_PREFILLED_SYRINGES=="" and DOCUMENT.XOLAIR_USE_ONLY_75MG<>""
						inj1="0.6 mL"
						inj2="0.6 mL"
						inj3="0.6 mL"
						DOCUMENT.XOLAIR1AMTVOL="75mg/0.6mL"
						DOCUMENT.XOLAIR2AMTVOL="75mg/0.6mL"
						DOCUMENT.XOLAIR3AMTVOL="75mg/0.6mL"
					case DOCUMENT.XOLAIR_PREFILLED_SYRINGES<>"" and DOCUMENT.XOLAIR_USE_ONLY_75MG==""
						inj1="1 mL (pre-filled syringe)"
						inj2="0.5 mL (pre-filled syringe)"
						DOCUMENT.XOLAIR1AMTVOL="150mg/1mL"
						DOCUMENT.XOLAIR2AMTVOL="75mg/0.5mL"
					case DOCUMENT.XOLAIR_PREFILLED_SYRINGES<>"" and DOCUMENT.XOLAIR_USE_ONLY_75MG<>""
						inj1="0.5 mL (pre-filled syringe)"
						inj2="0.5 mL (pre-filled syringe)"
						inj3="0.5 mL (pre-filled syringe)"
						DOCUMENT.XOLAIR1AMTVOL="75mg/0.5mL"
						DOCUMENT.XOLAIR2AMTVOL="75mg/0.5mL"
						DOCUMENT.XOLAIR3AMTVOL="75mg/0.5mL"
				endcond
				DOCUMENT.MILLIGRAMS_SELECTED=225
			case match(dosage,"300")>0
				cond
					case DOCUMENT.XOLAIR_PREFILLED_SYRINGES=="" and DOCUMENT.XOLAIR_USE_ONLY_75MG==""
						inj1="1.2 mL"
						inj2="1.2 mL"
						DOCUMENT.XOLAIR1AMTVOL="150mg/1.2mL"
						DOCUMENT.XOLAIR2AMTVOL="150mg/1.2mL"
					case DOCUMENT.XOLAIR_PREFILLED_SYRINGES=="" and DOCUMENT.XOLAIR_USE_ONLY_75MG<>""
						inj1="0.6 mL"
						inj2="0.6 mL"
						inj3="0.6 mL"
						inj4="0.6 mL"
						DOCUMENT.XOLAIR1AMTVOL="75mg/0.6mL"
						DOCUMENT.XOLAIR2AMTVOL="75mg/0.6mL"
						DOCUMENT.XOLAIR3AMTVOL="75mg/0.6mL"
						DOCUMENT.XOLAIR4AMTVOL="75mg/0.6mL"
					case DOCUMENT.XOLAIR_PREFILLED_SYRINGES<>"" and DOCUMENT.XOLAIR_USE_ONLY_75MG==""
						inj1="1 mL (pre-filled syringe)"
						inj2="1 mL (pre-filled syringe)"
						DOCUMENT.XOLAIR1AMTVOL="150mg/1mL"
						DOCUMENT.XOLAIR2AMTVOL="150mg/1mL"
					case DOCUMENT.XOLAIR_PREFILLED_SYRINGES<>"" and DOCUMENT.XOLAIR_USE_ONLY_75MG<>""
						inj1="0.5 mL (pre-filled syringe)"
						inj2="0.5 mL (pre-filled syringe)"
						inj3="0.5 mL (pre-filled syringe)"
						inj4="0.5 mL (pre-filled syringe)"
						DOCUMENT.XOLAIR1AMTVOL="75mg/0.5mL"
						DOCUMENT.XOLAIR2AMTVOL="75mg/0.5mL"
						DOCUMENT.XOLAIR3AMTVOL="75mg/0.5mL"
						DOCUMENT.XOLAIR4AMTVOL="75mg/0.5mL"
				endcond
				DOCUMENT.MILLIGRAMS_SELECTED=300
			case match(dosage,"375")>0
				cond
					case DOCUMENT.XOLAIR_PREFILLED_SYRINGES=="" and DOCUMENT.XOLAIR_USE_ONLY_75MG==""
						inj1="1.0 mL"
						inj2="1.0 mL"
						inj3="1.0 mL"
						DOCUMENT.XOLAIR1AMTVOL="150mg/1.2mL"
						DOCUMENT.XOLAIR2AMTVOL="150mg/1.2mL"
						DOCUMENT.XOLAIR3AMTVOL="150mg/1.2mL"
					case DOCUMENT.XOLAIR_PREFILLED_SYRINGES=="" and DOCUMENT.XOLAIR_USE_ONLY_75MG<>""
						inj1="0.6 mL"
						inj2="0.6 mL"
						inj3="0.6 mL"
						inj4="0.6 mL"
						inj5="0.6 mL"
						DOCUMENT.XOLAIR1AMTVOL="75mg/0.6mL"
						DOCUMENT.XOLAIR2AMTVOL="75mg/0.6mL"
						DOCUMENT.XOLAIR3AMTVOL="75mg/0.6mL"
						DOCUMENT.XOLAIR4AMTVOL="75mg/0.6mL"
						DOCUMENT.XOLAIR5AMTVOL="75mg/0.6mL"
					case DOCUMENT.XOLAIR_PREFILLED_SYRINGES<>"" and DOCUMENT.XOLAIR_USE_ONLY_75MG==""
						inj1="1 mL (pre-filled syringe)"
						inj2="1 mL (pre-filled syringe)"
						inj3="0.5 mL (pre-filled syringe)"
						DOCUMENT.XOLAIR1AMTVOL="150mg/1mL"
						DOCUMENT.XOLAIR2AMTVOL="150mg/1mL"
						DOCUMENT.XOLAIR3AMTVOL="75mg/0.5mL"
					case DOCUMENT.XOLAIR_PREFILLED_SYRINGES<>"" and DOCUMENT.XOLAIR_USE_ONLY_75MG<>""
						inj1="0.5 mL (pre-filled syringe)"
						inj2="0.5 mL (pre-filled syringe)"
						inj3="0.5 mL (pre-filled syringe)"
						inj4="0.5 mL (pre-filled syringe)"
						inj5="0.5 mL (pre-filled syringe)"
						DOCUMENT.XOLAIR1AMTVOL="75mg/0.5mL"
						DOCUMENT.XOLAIR2AMTVOL="75mg/0.5mL"
						DOCUMENT.XOLAIR3AMTVOL="75mg/0.5mL"
						DOCUMENT.XOLAIR4AMTVOL="75mg/0.5mL"
						DOCUMENT.XOLAIR5AMTVOL="75mg/0.5mL"
				endcond
				DOCUMENT.MILLIGRAMS_SELECTED=375
			case match(dosage,"450")>0
				cond
					case DOCUMENT.XOLAIR_PREFILLED_SYRINGES=="" and DOCUMENT.XOLAIR_USE_ONLY_75MG==""
						inj1="1.2 mL"
						inj2="1.2 mL"
						inj3="1.2 mL"
						DOCUMENT.XOLAIR1AMTVOL="150mg/1.2mL"
						DOCUMENT.XOLAIR2AMTVOL="150mg/1.2mL"
						DOCUMENT.XOLAIR3AMTVOL="150mg/1.2mL"
					case DOCUMENT.XOLAIR_PREFILLED_SYRINGES=="" and DOCUMENT.XOLAIR_USE_ONLY_75MG<>""
						inj1="0.6 mL"
						inj2="0.6 mL"
						inj3="0.6 mL"
						inj4="0.6 mL"
						inj5="0.6 mL"
						inj6="0.6 mL"
						DOCUMENT.XOLAIR1AMTVOL="75mg/0.6mL"
						DOCUMENT.XOLAIR2AMTVOL="75mg/0.6mL"
						DOCUMENT.XOLAIR3AMTVOL="75mg/0.6mL"
						DOCUMENT.XOLAIR4AMTVOL="75mg/0.6mL"
						DOCUMENT.XOLAIR5AMTVOL="75mg/0.6mL"
						DOCUMENT.XOLAIR6AMTVOL="75mg/0.6mL"
					case DOCUMENT.XOLAIR_PREFILLED_SYRINGES<>"" and DOCUMENT.XOLAIR_USE_ONLY_75MG==""
						inj1="1 mL (pre-filled syringe)"
						inj2="1 mL (pre-filled syringe)"
						inj3="1 mL (pre-filled syringe)"
						DOCUMENT.XOLAIR1AMTVOL="150mg/1mL"
						DOCUMENT.XOLAIR2AMTVOL="150mg/1mL"
						DOCUMENT.XOLAIR3AMTVOL="150mg/1mL"
					case DOCUMENT.XOLAIR_PREFILLED_SYRINGES<>"" and DOCUMENT.XOLAIR_USE_ONLY_75MG<>""
						inj1="0.5 mL (pre-filled syringe)"
						inj2="0.5 mL (pre-filled syringe)"
						inj3="0.5 mL (pre-filled syringe)"
						inj4="0.5 mL (pre-filled syringe)"
						inj5="0.5 mL (pre-filled syringe)"
						inj6="0.5 mL (pre-filled syringe)"
						DOCUMENT.XOLAIR1AMTVOL="75mg/0.5mL"
						DOCUMENT.XOLAIR2AMTVOL="75mg/0.5mL"
						DOCUMENT.XOLAIR3AMTVOL="75mg/0.5mL"
						DOCUMENT.XOLAIR4AMTVOL="75mg/0.5mL"
						DOCUMENT.XOLAIR5AMTVOL="75mg/0.5mL"
						DOCUMENT.XOLAIR6AMTVOL="75mg/0.5mL"
				endcond
				DOCUMENT.MILLIGRAMS_SELECTED=450
			case match(dosage,"600")>0
				if (DOCUMENT.XOLAIR_PREFILLED_SYRINGES=="") then
					inj1="1.2 mL"
					inj2="1.2 mL"
					inj3="1.2 mL"
					inj4="1.2 mL"
					DOCUMENT.XOLAIR1AMTVOL="150mg/1.2mL"
					DOCUMENT.XOLAIR2AMTVOL="150mg/1.2mL"
					DOCUMENT.XOLAIR3AMTVOL="150mg/1.2mL"
					DOCUMENT.XOLAIR4AMTVOL="150mg/1.2mL"
				else
					inj1="1 mL (pre-filled syringe)"
					inj2="1 mL (pre-filled syringe)"
					inj3="1 mL (pre-filled syringe)"
					inj4="1 mL (pre-filled syringe)"
					DOCUMENT.XOLAIR1AMTVOL="150mg/1mL"
					DOCUMENT.XOLAIR2AMTVOL="150mg/1mL"
					DOCUMENT.XOLAIR3AMTVOL="150mg/1mL"
					DOCUMENT.XOLAIR4AMTVOL="150mg/1mL"
				endif
				DOCUMENT.MILLIGRAMS_SELECTED=600
			case match(dosage,"750")>0
				if (DOCUMENT.XOLAIR_PREFILLED_SYRINGES=="") then
					inj1="1.2 mL"
					inj2="1.2 mL"
					inj3="1.2 mL"
					inj4="1.2 mL"
					inj5="1.2 mL"
					DOCUMENT.XOLAIR1AMTVOL="150mg/1.2mL"
					DOCUMENT.XOLAIR2AMTVOL="150mg/1.2mL"
					DOCUMENT.XOLAIR3AMTVOL="150mg/1.2mL"
					DOCUMENT.XOLAIR4AMTVOL="150mg/1.2mL"
					DOCUMENT.XOLAIR5AMTVOL="150mg/1.2mL"
				else
					inj1="1 mL (pre-filled syringe)"
					inj2="1 mL (pre-filled syringe)"
					inj3="1 mL (pre-filled syringe)"
					inj4="1 mL (pre-filled syringe)"
					inj5="1 mL (pre-filled syringe)"
					DOCUMENT.XOLAIR1AMTVOL="150mg/1mL"
					DOCUMENT.XOLAIR2AMTVOL="150mg/1mL"
					DOCUMENT.XOLAIR3AMTVOL="150mg/1mL"
					DOCUMENT.XOLAIR4AMTVOL="150mg/1mL"
					DOCUMENT.XOLAIR5AMTVOL="150mg/1mL"
				endif
				DOCUMENT.MILLIGRAMS_SELECTED=750
			case match(dosage,"75")>0
				DOCUMENT.MILLIGRAMS_SELECTED=75
				if (DOCUMENT.XOLAIR_PREFILLED_SYRINGES=="") then
					inj1="0.6 mL"
					DOCUMENT.XOLAIR1AMTVOL="75mg/0.6mL"
				else
					inj1="0.5 mL (pre-filled syringe)"
					DOCUMENT.XOLAIR1AMTVOL="75mg/0.5mL"
				endif
		endcond
		DOCUMENT.XOLAIR1DOSE=inj1
		DOCUMENT.XOLAIR2DOSE=inj2
		DOCUMENT.XOLAIR3DOSE=inj3
		DOCUMENT.XOLAIR4DOSE=inj4
		DOCUMENT.XOLAIR5DOSE=inj5
		DOCUMENT.XOLAIR6DOSE=inj6
		/*-----------------------*/
		/*Calculate Wasted Units (only if not using pre-filled syringes)
		/*-----------------------*/
		if DOCUMENT.XOLAIR_PREFILLED_SYRINGES=="" then
			local mgs=dosage
			if (size(mgs)>=3) then
				mgs=sub(dosage,1,3)
			endif
			local remainder=mod(val(mgs),150)
			local waste=remainder/5
			DOCUMENT.MILLIGRAMS_GIVEN=mgs /* save milligrams given */
			DOCUMENT.XOLAIRWASTED=str(waste)/* save wastage */
		else
			DOCUMENT.MILLIGRAMS_GIVEN=DOCUMENT.MILLIGRAMS_SELECTED /* save milligrams given */
			DOCUMENT.XOLAIRWASTED=""
		endif
	endif
}
/**************************************************************/
/* Function: fnBiologicsAdminXolairClearRows()
/* Purpose: Clear observations for rows given in calling args.
/**************************************************************/
!fn fnBiologicsAdminXolairClearRows()
{
	local i,n=getnargs()
	for i=1,i<=n,i=i+1
	do
		eval("DOCUMENT.XOLAIR"+str(i)+"DOSE=''")
		fnBiologicsAdminXolairClearThisObs("XOLAIR"+str(i)+"DOSE")
		fnBiologicsAdminXolairClearThisObs("XOLAIR"+str(i)+"STE")
		fnBiologicsAdminXolairClearThisObs("XOLAIR"+str(i)+"EXP")
		fnBiologicsAdminXolairClearThisObs("XOLAIR"+str(i)+"LOT")
	endfor
}
!fn fnBiologicsAdminXolairClearThisObs(obsName)
{
	OBSNOW(obsName,"")
}
/**************************************************************/
/* Function: fnBiologicsAdminXolairTranslation()
/* Purpose: Conditional translation of the Xolair tab if Injection Site has been entered.
/**************************************************************/
!fn fnBiologicsAdminXolairTranslation(done,vitals,buyAndBill,comments,indication,dosage,frequency,mfr,route,mgSelected,xolair1Dose,xolair1InjSite,xolair1Exp,xolair1Lot,xolair2Dose,xolair2InjSite,xolair2Exp,xolair2Lot,xolair3Dose,xolair3InjSite,xolair3Exp,xolair3Lot,xolair4Dose,xolair4InjSite,xolair4Exp,xolair4Lot,xolair5Dose,xolair5InjSite,xolair5Exp,xolair5Lot,xolair6Dose,xolair6InjSite,xolair6Exp,xolair6Lot,wastedUnits,epipenRx,xolairReviewed,waited,givenBy,nbrVials,ordered,pharmInfo,addtlCmts)
{
	local retStr=""
	/* If "Done" checkbox just checked, record date and time */
	if ok(done) and done<>"" and DOCUMENT.XOLAIR_DONE_DATE_TIME=="" then
		DOCUMENT.XOLAIR_DONE_DATE_TIME=DATETIMESTAMP()
	else if ok(done) and done=="" and DOCUMENT.XOLAIR_DONE_DATE_TIME<>"" then
			DOCUMENT.XOLAIR_DONE_DATE_TIME="" //user must have un-clicked the Done checkbox
		 endif
	endif
	/* Only do the translation if injection site#1 has been entered */
	if (done<>"" or xolair1InjSite<>"") then
		retStr=fmt("Xolair Administration ","B,2")+HRET+HRET
		retStr=retStr+CFMT(comments, "", "Med Comments / Instructions: ", "B", HRET)
		retStr=retStr+CFMT(indication, "", "Primary Indication: ", "B", HRET)
		retStr=retStr+CFMT(dosage, "", "Dosage: ", "B", HRET)
		retStr=retStr+CFMT(DOCUMENT.XOLAIR_DONE_DATE_TIME, "", "Done: ", "B", HRET)
		retStr=retStr+CFMT(frequency, "", "Frequency: ", "B", HRET)
		retStr=retStr+CFMT(mfr, "", "Manufacturer: ", "B", HRET)
		retStr=retStr+CFMT(route, "", "Route: ", "B", HRET)
		if (LAST_SIGNED_OBS_DATE("XOLAIR DOSE")<>"") then
			retStr=retStr+fmt("Date of last injection: ", "B")+sub(LAST_SIGNED_OBS_DATE("XOLAIR DOSE"),1,10)+HRET
		endif
		/* Vital Signs: */
		retStr=retStr+CFMT(vitals,"","","",HRET)
		/* Injection#1 */
		if (xolair1Dose<>"") then
			OBSNOW("XOLAIR DOSE",xolair1Dose)
			retStr=retStr+HRET+FMT("Injection # 1", "B,2")+HRET
			retStr=retStr+"Xolair "+DOCUMENT.XOLAIR1AMTVOL+HRET
			retStr=retStr+CFMT(xolair1Dose, "", "Amount/Volume: ","B", HRET)
			retStr=retStr+CFMT(xolair1InjSite, "", "Injection Site: ","B", HRET)
			retStr=retStr+CFMT(xolair1Exp,"","Expiration Date: ","B",HRET)
			retStr=retStr+CFMT(xolair1Lot,"","Lot #: ","B",HRET)
		endif
		/* Injection#2 */
		if (xolair2Dose<>"") then
			OBSNOW("XOLAIR2DOSE",xolair2Dose)
			retStr=retStr+HRET+FMT("Injection # 2", "B,2")+HRET
			retStr=retStr+"Xolair "+DOCUMENT.XOLAIR2AMTVOL+HRET
			retStr=retStr+CFMT(xolair2Dose, "", "Amount/Volume: ","B", HRET)
			retStr=retStr+CFMT(xolair2InjSite, "", "Injection Site: ","B", HRET)
			retStr=retStr+CFMT(xolair2Exp,"","Expiration Date: ","B",HRET)
			retStr=retStr+CFMT(xolair2Lot,"","Lot #: ","B",HRET)
		endif
		/* Injection#3 */
		if (xolair3Dose<>"") then
			OBSNOW("XOLAIR3DOSE",xolair3Dose)
			retStr=retStr+HRET+FMT("Injection # 3", "B,2")+HRET
			retStr=retStr+"Xolair "+DOCUMENT.XOLAIR3AMTVOL+HRET
			retStr=retStr+CFMT(xolair3Dose, "", "Amount/Volume: ","B", HRET)
			retStr=retStr+CFMT(xolair3InjSite, "", "Injection Site: ","B", HRET)
			retStr=retStr+CFMT(xolair3Exp,"","Expiration Date: ","B",HRET)
			retStr=retStr+CFMT(xolair3Lot,"","Lot #: ","B",HRET)
		endif
		/* Injection#4 */
		if (xolair4Dose<>"") then
			OBSNOW("XOLAIR4DOSE",xolair4Dose)
			retStr=retStr+HRET+FMT("Injection # 4", "B,2")+HRET
			retStr=retStr+"Xolair "+DOCUMENT.XOLAIR4AMTVOL+HRET
			retStr=retStr+CFMT(xolair4Dose, "", "Amount/Volume: ","B", HRET)
			retStr=retStr+CFMT(xolair4InjSite, "", "Injection Site: ","B", HRET)
			retStr=retStr+CFMT(xolair4Exp,"","Expiration Date: ","B",HRET)
			retStr=retStr+CFMT(xolair4Lot,"","Lot #: ","B",HRET)
		endif
		/* Injection#5 */
		if (xolair5Dose<>"") then
			OBSNOW("XOLAIR5DOSE",xolair5Dose)
			retStr=retStr+HRET+FMT("Injection # 5", "B,2")+HRET
			retStr=retStr+"Xolair "+DOCUMENT.XOLAIR5AMTVOL+HRET
			retStr=retStr+CFMT(xolair5Dose, "", "Amount/Volume: ","B", HRET)
			retStr=retStr+CFMT(xolair5InjSite, "", "Injection Site: ","B", HRET)
			retStr=retStr+CFMT(xolair5Exp,"","Expiration Date: ","B",HRET)
			retStr=retStr+CFMT(xolair5Lot,"","Lot #: ","B",HRET)
		endif
		/* Injection#6 */
		if (xolair6Dose<>"") then
			OBSNOW("XOLAIR6DOSE",xolair6Dose)
			retStr=retStr+HRET+FMT("Injection # 6", "B,2")+HRET
			retStr=retStr+"Xolair "+DOCUMENT.XOLAIR6AMTVOL+HRET
			retStr=retStr+CFMT(xolair6Dose, "", "Amount/Volume: ","B", HRET)
			retStr=retStr+CFMT(xolair6InjSite, "", "Injection Site: ","B", HRET)
			retStr=retStr+CFMT(xolair6Exp,"","Expiration Date: ","B",HRET)
			retStr=retStr+CFMT(xolair6Lot,"","Lot #: ","B",HRET)
		endif
		/* Wasted Units */
		if (wastedUnits<>"") then
			OBSNOW("XOLAIRWASTED",wastedUnits)
			retStr=retStr+HRET+fmt("Total Wasted Units: ", "B,2")+wastedUnits+HRET	
		else
			OBSNOW("XOLAIRWASTED","")
		endif
		/* Patient Notes */
		if (str(epipenRx,xolairReviewed,waited)<>"") then
			retStr=retStr+HRET+FMT("Patient Notes ", "B,2")+HRET
			retStr=retStr+CFMT(epipenRx, "", "Patient has an Epipen prescription? ", "B",HRET)
			retStr=retStr+CFMT(xolairReviewed, "", "Xolair medication guide has been reviewed by patient? ", "B",HRET)
			retStr=retStr+CFMT(waited, "", "Patient waited the prescribed amount of time with no reaction noted? ", "B",HRET)
		endif
		/* Given By */
		if (givenBy<>"") then
			retStr=retStr+CFMT(givenBy, "", "Injection(s) given by: ", "B",HRET)
		endif
		/* Tracking/Pharmacy Information */
		if (str(buyAndBill,nbrVials,ordered,pharmInfo)<>"") then
			retStr=retStr+HRET+fmt("Tracking/Pharmacy Information","B,2")+HRET
			retStr=retStr+CFMT(nbrVials, "", "Number of vials on hand after todays visit: ", "B",HRET)
			retStr=retStr+CFMT(ordered, "", "Ordered from pharmacy: ", "B",HRET)
			retStr=retStr+CFMT(buyAndBill, "", "Is this patient on the AP Biologics program? ", "B", HRET)
			retStr=retStr+CFMT(pharmInfo, "", "Xolair Pharmacy Information: ", "B",HRET)
		endif
		retStr=retStr+CFMT(addtlCmts, "", "Additional Comments: ", "B",HRET)
	endif
	return retStr
}
/*****************************************/
/*****************************************/
/* C I N Q A I R   S E C T I O N
/*****************************************/
/*****************************************/
/*****************************************/
/* Watcher to convert wieght in pounds to kilograms
/*****************************************/
{OBSNOW("WEIGHT (KG)",fnBiologicsAdminConvertLbsToKgs(OBSNOW("WEIGHT")))}
/*****************************************/
/* Function: fnBiologicsAdminConvertLbsToKgs(wt)
/* Purpose: Convert pounds to kilograms.
/*****************************************/
fn fnBiologicsAdminConvertLbsToKgs(wt)
{
	local retStr=""
	if (ok(wt) and wt<>"") then
		local wtKg=wt/2.2
		retStr=str(round(wtKg,0))
	endif
	return retStr
}
/*****************************************/
/* Function: fnBiologicsAdminCalcCinqairDose(wt)
/* Purpose: Calculate Cinqair dose based on today's
/*			patient weight at 3mg/kg.
/*****************************************/
fn fnBiologicsAdminCalcCinqairDose(wtKg)
{
	if (ok(wtKg) and wtKg<>"") then
		local dose=""
		DOCUMENT.CINQAIR_VIALS=""
		if (wtKg<35) then
			userok("cannot calculate dose because weight is less than 35 kg")
			return ""
		endif
		if (wtKg>140) then
			userok("cannot calculate dose because weight is greater than 140 kg")
			return ""
		endif
		local mg=wtKg*3
		local mL=mg/10
		local sMg=round(mg,0)
		dose=sMg+" mg ("+round(mL,1)+" mL of 100mg/10mL solution)"
		local numVials=""
		cond
			case mL<=20 numVials=2
			case mL<=30 numVials=3
			case mL<=40 numVials=4
			case mL<=42 numVials=5
		endcond
		DOCUMENT.CINQAIR_VIALS=numVials
		OBSNOW("CINQAIR DOS",dose)
	else
		userok("Please enter patient's weight.")
		JUMP_TO_TAB("VITALS")
	endif
	return retStr
}
/**************************************************************/
/* Function: fnBiologicsAdminCinqairTranslation()
/* Purpose: Conditional translation of the Cinqair tab if the Done checkbox is checked.
/* Note:	 This function is called from the Cinqair Administration text component
/*			 translation area, with OBSNOW() values for most of the args.
/**************************************************************/
fn fnBiologicsAdminCinqairTranslation(done,vitals,indication,buyAndBill,medCmtsInstrs,freq,dose,route,lot,exp,mfr,site,givenBy,fluids,supMeds,supDose,access,start,end,duration,rate,vol,comments,consent,wait,rxn,medsDel,ptInstruct,pharmInfo)
{
	local retStr=""
	if ok(done) and done<>"" and DOCUMENT.CINQAIR_DONE_DATE_TIME=="" then
		DOCUMENT.CINQAIR_DONE_DATE_TIME=DATETIMESTAMP()
	else if ok(done) and done=="" and DOCUMENT.CINQAIR_DONE_DATE_TIME<>"" then
			DOCUMENT.CINQAIR_DONE_DATE_TIME="" //user must have un-clicked the Done checkbox
		 endif
	endif
	/* Only do the translation if the "done" checkbox is checked */
	if (done<>"") then
		/* continue building translation */
		retStr=fmt("Cinqair Administration ","B,2")+HRET
		/* Vital Signs */
		retStr=retStr+CFMT(vitals,"","","",HRET)
		retStr=retStr+CFMT(medCmtsInstrs, "", "Med Comments/Instructions: ", "B",HRET+HRET)
		retStr=retStr+CFMT(indication, "", "Primary Indication: ", "B", HRET)
		/* Injection */
		retStr=retStr+fmt("Cinqair Injection: ","B")+HRET
		retStr=retStr+CFMT(dose, "", "Dose: ", "B",HRET)
		retStr=retStr+CFMT(DOCUMENT.CINQAIR_DONE_DATE_TIME, "", "Done: ", "B",HRET)
		retStr=retStr+CFMT(route, "", "Route: ", "B",HRET)
		retStr=retStr+CFMT(mfr, "", "Manufacturer: ", "B",HRET)
		retStr=retStr+CFMT(lot, "", "Lot: ", "B",HRET)
		retStr=retStr+CFMT(exp, "", "Expiration: ", "B",HRET)
		retStr=retStr+CFMT(site, "", "Site: ", "B",HRET)
		retStr=retStr+CFMT(givenBy, "", "Administered by: ", "B",HRET)
		/* Support */
		retStr=retStr+CFMT(fluids, "", "Fluids: ", "B",HRET)
		retStr=retStr+CFMT(supMeds, "", "Supportive Meds: ", "B",HRET)
		retStr=retStr+CFMT(supDose, "", "Dose: ", "B",HRET)
		retStr=retStr+CFMT(access, "", "Access: ", "B",HRET)
		retStr=retStr+CFMT(start, "", "Start Time: ", "B",HRET)
		retStr=retStr+CFMT(end, "", "End Time: ", "B",HRET)
		retStr=retStr+CFMT(duration, "", "Duration: ", "B",HRET)
		retStr=retStr+CFMT(rate, "", "Rate Changes: ", "B",HRET)
		retStr=retStr+CFMT(vol, "", "Volume Changes: ", "B",HRET)
		retStr=retStr+CFMT(comments, "", "Comments: ", "B",HRET)
		/* Patient Notes */
		if (str(consent,wait)<>"") then
			retStr=retStr+HRET+fmt("Patient Notes","B,2")+HRET
			retStr=retStr+CFMT(consent, "", "Cinqair medication consent has been reviewed by patient? ", "B",HRET)
			retStr=retStr+CFMT(wait, "", "Patient waited the prescribed amount of time with no reaction noted? ", "B",HRET)
			if (wait=="no") then
				retStr=retStr+CFMT(rxn, "", "Reaction: ", "B",HRET)
			endif
		endif
		/* Tracking/Pharmacy Information */
		if (str(medsDel,ptInstruct,pharmInfo,freq,buyAndBill)<>"") then
			retStr=retStr+HRET+fmt("Tracking/Pharmacy Information","B,2")+HRET
			if (medsDel<>"") then
				retStr=retStr+"Medication was delivered to the patient. "+HRET
			endif
			if (ptInstruct<>"") then
				retStr=retStr+"The patient was instructed to contact the pharmacy for next dose. "+HRET
			endif
			retStr=retStr+CFMT(pharmInfo, "", "Cinqair Pharmacy Information: ", "B",HRET)
			retStr=retStr+CFMT(buyAndBill, "", "Is this patient on the AP Biologics program? ", "B", HRET)
			retStr=retStr+CFMT(freq, "", "Injection Frequency: ", "B",HRET)
		endif
	endif
	return retStr
}
/*****************************************/
/*****************************************/
/* D U P I X E N T   S E C T I O N
/*****************************************/
/*****************************************/
/**************************************************************/
/* Function: fnBiologicsAdminDupixentTranslation()
/* Purpose: Conditional translation of the Dupixent tab if the Done checkbox is checked.
/* Note:	 This function is called from the Dupixent Administration text component
/*			 translation area, with OBSNOW() values for most of the args.
/**************************************************************/
fn fnBiologicsAdminDupixentTranslation(done,vitals,buyAndBill,medCmtsInstrs,freq,dose,lot,route,exp,mfr,site,givenBy,ptUnable,reason,consent,wait,rxn,medsDel,ptInstruct,pharmInfo)
{
	local retStr=""
	if ok(done) and done<>"" and DOCUMENT.DUPIXENT_DONE_DATE_TIME=="" then
		DOCUMENT.DUPIXENT_DONE_DATE_TIME=DATETIMESTAMP()
	else if ok(done) and done=="" and DOCUMENT.DUPIXENT_DONE_DATE_TIME<>"" then
			DOCUMENT.DUPIXENT_DONE_DATE_TIME="" //user must have un-clicked the Done checkbox
		 endif
	endif
	/* Only do the translation if the "done" checkbox is checked */
	if (done<>"") then
		/* Default route and manufacturer if OBSNOW() is empty */
		route=fnBiologicsAdminDefaultIfEmpty("DUPIXENT RTE","Subcutaneous")
		mfr=fnBiologicsAdminDefaultIfEmpty("DUPIXENT MFR","Regeneron Sanofi Genzyme")
		/* continue building translation */
		retStr=fmt("Dupixent Administration ","B,2")+HRET
		/* Vital Signs */
		retStr=retStr+CFMT(vitals,"","","",HRET)
		/* Injection */
		retStr=retStr+fmt("Dupixent Injection #1:","B")+HRET
		retStr=retStr+CFMT(DOCUMENT.DUPIXENT_DONE_DATE_TIME, "", "Done: ", "B",HRET)
		retStr=retStr+CFMT(dose, "", "Dose: ", "B",HRET)
		retStr=retStr+CFMT(lot, "", "Lot: ", "B",HRET)
		retStr=retStr+CFMT(route, "", "Route: ", "B",HRET)
		retStr=retStr+CFMT(exp, "", "Expiration: ", "B",HRET)
		retStr=retStr+CFMT(mfr, "", "Manufacturer: ", "B",HRET)
		retStr=retStr+CFMT(site, "", "Site: ", "B",HRET)
		retStr=retStr+CFMT(givenBy, "", "Administered by: ", "B",HRET)
		/* Patient Notes */
		if (str(ptUnable,reason,consent,wait)<>"") then
			retStr=retStr+HRET+fmt("Patient Notes","B,2")+HRET
			if (ptUnable<>"") then
				retStr=retStr+ptUnable
				if (reason<>"") then
					retStr=retStr+"Reason: "+reason+"."
				endif
				retStr=retStr+HRET
			endif
			retStr=retStr+CFMT(consent, "", "Dupixent medication consent has been reviewed by patient? ", "B",HRET)
			if (wait<>"") then
				retStr=retStr+CFMT(wait, "", "Patient waited the prescribed amount of time with no reaction noted? ", "B",HRET)
				/* special processing to attempt to set ILARIS REACT to a meaningful value */
				if (wait=="yes") then
					OBSNOW("DUPIXENT RXN","no")
				else
					if (rxn<>"") then
						OBSNOW("DUPIXENT RXN",rxn)
						retStr=retStr+CFMT(rxn, "", "Reaction: ", "B",HRET)
					else
						OBSNOW("DUPIXENT RXN","yes")
					endif
				endif
			endif
		endif
		/* Tracking/Pharmacy Information */
		if (str(medsDel,ptInstruct,pharmInfo,freq,buyAndBill)<>"") then
			retStr=retStr+HRET+fmt("Tracking/Pharmacy Information","B,2")+HRET
			if (medsDel<>"") then
				retStr=retStr+fmt("Medication was delivered to the patient.","B")+HRET
			endif
			if (ptInstruct<>"") then
				retStr=retStr+fmt("The patient was instructed to contact the pharmacy for next dose.","B")+HRET
			endif
			retStr=retStr+CFMT(pharmInfo, "", "Dupixent Pharmacy Information: ", "B",HRET)
			retStr=retStr+CFMT(buyAndBill, "", "Is this patient on the AP Biologics program? ", "B", HRET)
			retStr=retStr+HRET+CFMT(medCmtsInstrs, "", "Med Comments/Instructions: ", "B",HRET)
			retStr=retStr+CFMT(freq, "", "Injection Frequency: ", "B",HRET)
		endif
	endif
	return retStr
}
/*****************************************/
/*****************************************/
/* F A S E N R A   S E C T I O N
/*****************************************/
/*****************************************/
/**************************************************************/
/* Function: fnBiologicsAdminFasenraTranslation()
/* Purpose: Conditional translation of the Fasenra tab if the Done checkbox is checked.
/**************************************************************/
fn fnBiologicsAdminFasenraTranslation(done,vitals,indication,buyAndBill,medCmtsInstrs,freq,dose,lot,route,exp,mfr,site,givenBy,consent,wait,rxn,nbrVials,ordered,pharmInfo,returnCB)
{
	local retStr=""
	if ok(done) and done<>"" and DOCUMENT.FASENRA_DONE_DATE_TIME=="" then
		DOCUMENT.FASENRA_DONE_DATE_TIME=DATETIMESTAMP()
	else if ok(done) and done=="" and DOCUMENT.FASENRA_DONE_DATE_TIME<>"" then
			DOCUMENT.FASENRA_DONE_DATE_TIME="" //user must have un-clicked the Done checkbox
		 endif
	endif
	/* Only do the translation if the "done" checkbox is checked */
	if (done<>"") then
		/* Default dose, route and manufacturer if OBSNOW() is empty */
		dose=fnBiologicsAdminDefaultIfEmpty("FASENRA DOSE","30 mg")
		route=fnBiologicsAdminDefaultIfEmpty("FASENRA RTE","Subcutaneous")
		mfr=fnBiologicsAdminDefaultIfEmpty("FASENRA MFR","AstraZeneca")
		/* continue building translation */
		retStr=fmt("Fasenra Administration ","B,2")+HRET
		/* Vital Signs */
		retStr=retStr+CFMT(vitals,"","","",HRET)
		retStr=retStr+CFMT(indication, "", "Primary Indication: ", "B", HRET)
		/* Injection */
		retStr=retStr+FMT("Fasenra Injection:  ","B")+HRET
		retStr=retStr+CFMT(dose, "", "Dose: ", "B",HRET)
		retStr=retStr+CFMT(DOCUMENT.FASENRA_DONE_DATE_TIME, "", "Done: ", "B",HRET)
		retStr=retStr+CFMT(lot, "", "Lot: ", "B",HRET)
		retStr=retStr+CFMT(exp, "", "Expiration: ", "B",HRET)
		retStr=retStr+CFMT(mfr, "", "Manufacturer: ", "B",HRET)
		retStr=retStr+CFMT(route, "", "Route: ", "B",HRET)
		retStr=retStr+CFMT(site, "", "Site: ", "B",HRET)
		retStr=retStr+CFMT(givenBy, "", "Administered by: ", "B",HRET)
		/* Patient Notes */
		if (str(consent,wait,returnCB)<>"") then
			retStr=retStr+HRET+fmt("Patient Notes","B,2")+HRET
			retStr=retStr+CFMT(consent, "", "Fasenra medication consent has been reviewed by patient? ", "B",HRET)
			if (wait<>"") then
				retStr=retStr+CFMT(wait, "", "Patient waited the prescribed amount of time with no reaction noted? ", "B",HRET)
				/* special processing to attempt to set FASENRA RXN to a meaningful value */
				if (wait=="yes") then
					OBSNOW("FASENRA RXN","No reaction")
				else
					if (rxn<>"") then
						OBSNOW("FASENRA RXN",rxn)
						retStr=retStr+CFMT(rxn, "", "Reaction: ", "B",HRET)
					else
						OBSNOW("FASENRA RXN","Had reaction")
					endif
				endif
			endif
		endif
		retStr=retStr+CFMT(DOCUMENT.FASENRA_RETURN_CB, "", "", "B", HRET)
		/* Tracking/Pharmacy Information */
		if (str(buyAndBill,nbrVials,ordered,pharmInfo,buyAndBill)<>"") then
			retStr=retStr+HRET+fmt("Tracking/Pharmacy Information","B,2")+HRET
			retStr=retStr+CFMT(nbrVials, "", "Number of vials on hand after todays visit: ", "B",HRET)
			retStr=retStr+CFMT(ordered, "", "Ordered from pharmacy: ", "B",HRET)
			retStr=retStr+CFMT(buyAndBill, "", "Is this patient on the AP Biologics program? ", "B", HRET)
			retStr=retStr+CFMT(pharmInfo, "", "Fasenra Pharmacy Information: ", "B",HRET)
		endif
		retStr=retStr+CFMT(medCmtsInstrs, "",HRET+"Med Comments/Instructions: ", "B",HRET)
		retStr=retStr+CFMT(freq, "", "Fasenra injection frequency: ", "B",HRET)
	endif
	return retStr
}

/*****************************************/
/*****************************************/
/* I L A R I S   S E C T I O N
/*****************************************/
/*****************************************/
/**************************************************************/
/* Function: fnBiologicsAdminIlarisTranslation()
/* Purpose: Conditional translation of the Ilaris tab if the Done checkbox is checked.
/* Note:	 This function is called from the Ilaris Administration text component
/*			 translation area, with OBSNOW() values for most of the args.
/**************************************************************/
fn fnBiologicsAdminIlarisTranslation(done,vitals,buyAndBill,medCmtsInstrs,freq,dose,lot,route,exp,mfr,site,givenBy,consent,wait,rxn,nbrVials,ordered,pharmInfo)
{
	local retStr=""
	if ok(done) and done<>"" and DOCUMENT.ILARIS_DONE_DATE_TIME=="" then
		DOCUMENT.ILARIS_DONE_DATE_TIME=DATETIMESTAMP()
	else if ok(done) and done=="" and DOCUMENT.ILARIS_DONE_DATE_TIME<>"" then
			DOCUMENT.ILARIS_DONE_DATE_TIME="" //user must have un-clicked the Done checkbox
		 endif
	endif
	/* Only do the translation if the "done" checkbox is checked */
	if (done<>"") then
		/* Default dose, route and manufacturer if OBSNOW() is empty */
		dose=fnBiologicsAdminDefaultIfEmpty("ILARIS DOSE","150 mg")
		route=fnBiologicsAdminDefaultIfEmpty("ILARIS RTE","Subcutaneous")
		mfr=fnBiologicsAdminDefaultIfEmpty("ILARIS MFR","Novartis")
		/* continue building translation */
		retStr=fmt("Ilaris Administration ","B,2")+HRET
		/* Vital Signs */
		retStr=retStr+CFMT(vitals,"","","",HRET)
		/* Injection */
		retStr=retStr+fmt("Ilaris Injection #1:  ","B")+HRET
		retStr=retStr+CFMT(dose, "", "Dose: ", "B",HRET)
		retStr=retStr+CFMT(DOCUMENT.ILARIS_DONE_DATE_TIME, "", "Done: ", "B",HRET)
		retStr=retStr+CFMT(lot, "", "Lot: ", "B",HRET)
		retStr=retStr+CFMT(exp, "", "Expiration: ", "B",HRET)
		retStr=retStr+CFMT(mfr, "", "Manufacturer: ", "B",HRET)
		retStr=retStr+CFMT(route, "", "Route: ", "B",HRET)
		retStr=retStr+CFMT(site, "", "Site: ", "B",HRET)
		retStr=retStr+CFMT(givenBy, "", "Administered by: ", "B",HRET)
		/* Patient Notes */
		if (str(consent,wait)<>"") then
			retStr=retStr+HRET+fmt("Patient Notes","B,2")+HRET
			retStr=retStr+CFMT(consent, "", "Ilaris medication consent has been reviewed by patient? ", "B",HRET)
			if (wait<>"") then
				retStr=retStr+CFMT(wait, "", "Patient waited the prescribed amount of time with no reaction noted? ", "B",HRET)
				/* special processing to attempt to set ILARIS REACT to a meaningful value */
				if (wait=="yes") then
					OBSNOW("ILARIS REACT","no")
				else
					if (rxn<>"") then
						OBSNOW("ILARIS REACT",rxn)
						retStr=retStr+CFMT(rxn, "", "Reaction: ", "B",HRET)
					else
						OBSNOW("ILARIS REACT","yes")
					endif
				endif
			endif
		endif
		/* Tracking/Pharmacy Information */
		if (str(buyAndBill,nbrVials,ordered,pharmInfo,buyAndBill)<>"") then
			retStr=retStr+HRET+fmt("Tracking/Pharmacy Information","B,2")+HRET
			retStr=retStr+CFMT(nbrVials, "", "Number of vials on hand after todays visit: ", "B",HRET)
			retStr=retStr+CFMT(ordered, "", "Ordered from pharmacy: ", "B",HRET)
			retStr=retStr+CFMT(buyAndBill, "", "Is this patient on the AP Biologics program? ", "B", HRET)
			retStr=retStr+CFMT(pharmInfo, "", "Ilaris Pharmacy Information: ", "B",HRET)
		endif
		retStr=retStr+CFMT(medCmtsInstrs, "",HRET+"Med Comments/Instructions: ", "B",HRET)
		retStr=retStr+CFMT(freq, "", "Ilaris injection frequency: ", "B",HRET)
	endif
	return retStr
}
/*****************************************/
/*****************************************/
/* N U C A L A   S E C T I O N
/*****************************************/
/*****************************************/
/**************************************************/
/* Watcher to calculate Nucala amounts from selected dosage
/**************************************************/
{fnBiologicsAdminNucalaCalcAmt(OBSANY("NUCALA1INJ"))}
/**************************************************************/
/* Function: fnBiologicsAdminNucalaCalcAmt(dosage)
/* Purpose: Called from watcher above whenever user selects a Nucala dosage
/**************************************************************/
!fn fnBiologicsAdminNucalaCalcAmt(dosage)
{
	if (ok(dosage))then
		cond
			/* If user cleared dosage, just clear all dosing obs and return */
			case dosage==""
				DOCUMENT.NUCALA1DOSE=""
				DOCUMENT.NUCALA2DOSE=""
				DOCUMENT.NUCALA3DOSE=""
				DOCUMENT.NUCALAWASTED=""
			/* If user entered a dosage, calculate amounts */
			case dosage=="40 mg" 
				DOCUMENT.NUCALA1DOSE="0.4 mL"
				DOCUMENT.NUCALA2DOSE=""
				DOCUMENT.NUCALA3DOSE=""
				DOCUMENT.NUCALAWASTED="60"
			case dosage=="100 mg" 
				DOCUMENT.NUCALA1DOSE="1 mL"
				DOCUMENT.NUCALA2DOSE=""
				DOCUMENT.NUCALA3DOSE=""
				DOCUMENT.NUCALAWASTED=""
			case dosage=="300 mg" 
				DOCUMENT.NUCALA1DOSE="1 mL"
				DOCUMENT.NUCALA2DOSE="1 mL"
				DOCUMENT.NUCALA3DOSE="1 mL"
				DOCUMENT.NUCALAWASTED=""
		endcond
	endif
}
/**************************************************************/
/* Function: fnBiologicsAdminNucalaTranslation()
/* Purpose: Condition translation of the Nucala tab if the Done checkbox is checked.
/* Note:	 This function is called from the Nucala Administration text component
/*			 translation area, with OBSNOW() values for most of the args.
/**************************************************************/
fn fnBiologicsAdminNucalaTranslation(done,vitals,indication,buyAndBill,medCmtsInstrs,freq,dosage,dose1,lot1,route,exp1,mfr,site1,givenBy,consent,wait,rxn,nbrVials,ordered,pharmInfo,dose2,site2,lot2,exp2,dose3,site3,lot3,exp3,returnCB)
{
	local retStr=""
	if ok(done) and done<>"" and DOCUMENT.NUCALA_DONE_DATE_TIME=="" then
		DOCUMENT.NUCALA_DONE_DATE_TIME=DATETIMESTAMP()
	else if ok(done) and done=="" and DOCUMENT.NUCALA_DONE_DATE_TIME<>"" then
			DOCUMENT.NUCALA_DONE_DATE_TIME="" //user must have un-clicked the Done checkbox
		 endif
	endif
	/* Only do the translation if the "done" checkbox is checked */
	if (done<>"") then
		/* Default dose, route and manufacturer if OBSNOW() is empty */
		dose=fnBiologicsAdminDefaultIfEmpty("NUCALA1INJ","100 mg")
		route=fnBiologicsAdminDefaultIfEmpty("NUCALA1RTE","Subcutaneous")
		mfr=fnBiologicsAdminDefaultIfEmpty("NUCALA MANF","GlaxoSmithKline")
		/* continue building translation */
		retStr=fmt("Nucala Administration ","B,2")+HRET
		retStr=retStr+CFMT(medCmtsInstrs, "", "Med Comments / Instructions: ", "B", HRET)
		retStr=retStr+CFMT(indication, "", "Primary Indication: ", "B", HRET)
		retStr=retStr+CFMT(dosage, "", "Dosage: ", "B", HRET)
		retStr=retStr+CFMT(DOCUMENT.NUCALA_DONE_DATE_TIME, "", "Done: ", "B",HRET)
		retStr=retStr+CFMT(freq, "", "Frequency: ", "B", HRET)
		retStr=retStr+CFMT(mfr, "", "Manufacturer: ", "B", HRET)
		retStr=retStr+CFMT(route, "", "Route: ", "B", HRET)
		if (LAST_SIGNED_OBS_DATE("NUCALA DOSE")<>"") then
			retStr=retStr+fmt("Date of last injection: ", "B")+sub(LAST_SIGNED_OBS_DATE("NUCALA DOSE"),1,10)+HRET
		endif
		/* Vital Signs */
		retStr=retStr+CFMT(vitals,"","","",HRET)
		/* Injection */
		OBSNOW("NUCALA DOSE",dose1)
		retStr=retStr+CFMT(done, "", "Nucala Injection #1:  ","B"," on "+sub(str(DOCUMENT.CLINICALDATE),1,10)+HRET)
		retStr=retStr+CFMT(dose1, "", "Dose: ", "B",HRET)
		retStr=retStr+CFMT(lot1, "", "Lot: ", "B",HRET)
		retStr=retStr+CFMT(exp1, "", "Expiration: ", "B",HRET)
		retStr=retStr+CFMT(site1, "", "Site: ", "B",HRET)
		if (ok(site2) and site2<>"") then
			OBSNOW("NUCALA2DOSE",dose2)
			retStr=retStr+CFMT(done, "", "Nucala Injection #2:  ","B"," on "+sub(str(DOCUMENT.CLINICALDATE),1,10)+HRET)
			retStr=retStr+CFMT(dose2, "", "Dose: ", "B",HRET)
			retStr=retStr+CFMT(lot2, "", "Lot: ", "B",HRET)
			retStr=retStr+CFMT(exp2, "", "Expiration: ", "B",HRET)
			retStr=retStr+CFMT(site2, "", "Site: ", "B",HRET)
		endif
		if (ok(site3) and site3<>"") then
			OBSNOW("NUCALA3DOSE",dose3)
			retStr=retStr+CFMT(done, "", "Nucala Injection #3:  ","B"," on "+sub(str(DOCUMENT.CLINICALDATE),1,10)+HRET)
			retStr=retStr+CFMT(dose3, "", "Dose: ", "B",HRET)
			retStr=retStr+CFMT(lot3, "", "Lot: ", "B",HRET)
			retStr=retStr+CFMT(exp3, "", "Expiration: ", "B",HRET)
			retStr=retStr+CFMT(site3, "", "Site: ", "B",HRET)
		endif
		retStr=retStr+CFMT(givenBy, "", "Administered by: ", "B",HRET)
		/* Patient Notes */
		if (str(consent,wait,returnCB)<>"") then
			retStr=retStr+HRET+fmt("Patient Notes","B,2")+HRET
			retStr=retStr+CFMT(consent, "", "Nucala medication consent has been reviewed by patient? ", "B",HRET)
			if (wait<>"") then
				retStr=retStr+CFMT(wait, "", "Patient waited the prescribed amount of time with no reaction noted? ", "B",HRET)
				/* special processing to attempt to set NUCALA REACT to a meaningful value */
				if (wait=="yes") then
					OBSNOW("NUCALAREACT","No reaction")
				else
					if (rxn<>"") then
						OBSNOW("NUCALAREACT",rxn)
						retStr=retStr+CFMT(rxn, "", "Reaction: ", "B",HRET)
					else
						OBSNOW("NUCALAREACT","Had reaction")
					endif
				endif
			endif
			retStr=retStr+CFMT(DOCUMENT.NUCALA_RETURN_CB, "", "", "B", HRET)
		endif
		/* Tracking/Pharmacy Information */
		if (str(buyAndBill,nbrVials,ordered,pharmInfo,buyAndBill)<>"") then
			retStr=retStr+HRET+fmt("Tracking/Pharmacy Information","B,2")+HRET
			retStr=retStr+CFMT(nbrVials, "", "Number of vials on hand after todays visit: ", "B",HRET)
			retStr=retStr+CFMT(ordered, "", "Ordered from pharmacy: ", "B",HRET)
			retStr=retStr+CFMT(buyAndBill, "", "Is this patient on the AP Biologics program? ", "B", HRET)
			retStr=retStr+CFMT(pharmInfo, "", "Nucala Pharmacy Information: ", "B",HRET)
		endif
	endif
	return retStr
}

/**************************************************************/
/* Function: fnBiologicsAdminTranslate(label,listbox,editField)
/* Purpose: Translate a listbox and "other" editfield.
/**************************************************************/
fn fnBiologicsAdminTranslate(label,listbox,editField)
{
	local retStr=""
	if ((ok(listbox) and listbox<>"") or (ok(editField) and editField<>"")) then
		if (ok(listbox) and listbox<>"") then
			retStr=listbox
		endif
		if (ok(editField) and editField<>"") then
			if (retStr<>"") then
				retStr=retStr+", "
			endif
			retStr=retStr+editField
		endif
	endif
	if (retStr<>"") then
		retStr=fmt(label,"B")+retStr+HRET
	endif
	return retStr
}
/**************************************************************/
/* Function: fnBiologicsAdminDefaultIfEmpty(obsName,default)
/* Purpose: If OBSNOW(obsName) is empty, set it either to the last saved
/*			value or, if none, the passed-in default arg, which is
/*			also returned to the caller.
/**************************************************************/
fn fnBiologicsAdminDefaultIfEmpty(obsName,default)
{
	local obsVal=""
	if (OBSNOW(obsName)=="") then
		if (OBSPREV(obsName)<>"") then
			obsVal=OBSPREV(obsName)
		else
			obsVal=default
		endif
		OBSNOW(obsName,obsVal)
	else
		obsVal=OBSNOW(obsName)
	endif
	return obsVal
}
/**************************************************************/
/* Function: fnBiologicsAdminProcessThisOrderCheckbox()
/* Purpose: Call MEL_ADD_ORDER and display any error messages.
/**************************************************************/
fn fnBiologicsAdminProcessThisOrderCheckbox() 
{
	/* Is a drug selected? */
	if (DOCUMENT.SELECT_DRUG=="") then
		userok("Please select a drug")
		return ""
	endif
	/* Is Admin Code selected? */
	if (DOCUMENT.ADMIN_CODE=="") then
		userok("Please select an Injection Admin code")
		return ""
	endif
	local dxCode=fnBiologicsAdminGetProblemCodes()
	local dxDescr=fnBiologicsAdminGetProblemDescriptions()
	/* Does injection have an associated problem? */
	if (dxCode=="" or dxDescr==0) then
		userok("Please select a diagnosis")
		return ""
	endif
	local provider=fnSrvPhys(DOCUMENT.RESPONSIBLE)
	local description="",category=""
	if (DOCUMENT.SELECT_DRUG=="CINQAIR") then
		category="Drugs/Injectables/Infusion"
		if (match(DOCUMENT.ADMIN_CODE,"96365")>0) then
			description="INFUSION, IV therapy, prophylaxis, or diagnosis, up to 1 hour"
		else
			description="Chemo/Biologic admin, intravenous infusion;up to 1 hour, single drug"
		endif
	else
		category="Procedures/Labs/Vaccines"
		if (match(DOCUMENT.ADMIN_CODE,"96372")>0) then
			description="Therapeutic Prophylactic Inj  (subcu or IM)"
		else
			description="Injection Admin, subcutaneous or intramuscular; non-hormonal anti-neoplastic"
		endif
	endif
	/* Check to see if the order has already been entered today */
	local okToUse="Yes"
	if (match(ORDERS_NEW(),description)>0) then
		okToUse=useryesno("Order '"+description+"' is already in today's orders, do you want to add it anyway?")
	endif
	if (okToUse=="Yes") then
		local retcode=""
		/* Create the order */
		retcode = MEL_ADD_ORDER("S",category,description,"",dxCode,dxDescr,"","","",provider,"")
		if (retcode<>"0") then
			userok("Error calling MEL_ADD_ORDER. Error code "+fnBiologicsAdminDecodeError(retcode))
		endif
	endif
	/* If Buy-and-Bill is "Yes", add a charge order for the drug */
	local units="1" /* default units to "1" */
	description=""
	local description2="",units2="" /* needed for xolair pre-filled syringes */
	cond
		case DOCUMENT.SELECT_DRUG=="XOLAIR" and match(OBSANY("XOLAIR BNB"),"Yes")>0
			if (OBSANY("XOLAIR INJ")=="") then
				userok("No Xolair dose found")
				return ""
			endif
			if DOCUMENT.XOLAIR_PREFILLED_SYRINGES<>"" then
				cond
					case OBSANY("XOLAIR INJ")=="75 mgs"
						description="Omalizumab, 5 mg (Xolair) Prefilled 75 Syringe"
						units="1"
					case OBSANY("XOLAIR INJ")=="150 mgs"
						description="Omalizumab, 5 mg (Xolair) Prefilled 150 syringe"
						units="1"
					case OBSANY("XOLAIR INJ")=="225 mgs"
						description="Omalizumab, 5 mg (Xolair) Prefilled 150 syringe"
						units="1"
						description2="Omalizumab, 5 mg (Xolair) Prefilled 75 Syringe"
						units2="1"
					case OBSANY("XOLAIR INJ")=="300 mgs"
						description="Omalizumab, 5 mg (Xolair) Prefilled 150 syringe"
						units="2"
					case OBSANY("XOLAIR INJ")=="375 mgs"
						description="Omalizumab, 5 mg (Xolair) Prefilled 150 syringe"
						units="2"
						description2="Omalizumab, 5 mg (Xolair) Prefilled 75 Syringe"
						units2="1"
					case OBSANY("XOLAIR INJ")=="450 mgs"
						description="Omalizumab, 5 mg (Xolair) Prefilled 150 syringe"
						units="3"
					case OBSANY("XOLAIR INJ")=="600 mgs"
						description="Omalizumab, 5 mg (Xolair) Prefilled 150 syringe"
						units="4"
					case OBSANY("XOLAIR INJ")=="750 mgs"
						description="Omalizumab, 5 mg (Xolair) Prefilled 150 syringe"
						units="5"
				endcond
			else
				description="Omalizumab, 5 mg (Xolair)"
				units=str(val(fnBiologicsAdminStripNonNumerics(OBSANY("XOLAIR INJ")))/5)
			endif
		case DOCUMENT.SELECT_DRUG=="FASENRA" and match(OBSANY("FASENRA BNB"),"Yes")>0
			description="Injection, benralizumab, 1 mg"
			units="1"
		case DOCUMENT.SELECT_DRUG=="CINQAIR" and match(OBSANY("CINQAIR BNB"),"Yes")>0
			description="Injection, reslizumab, 1 mg"
			units=DOCUMENT.CINQAIR_VIALS
		case DOCUMENT.SELECT_DRUG=="DUPIXENT" and match(OBSANY("DUPIXENT BNB"),"Yes")>0
			description="Dupixent (Dupilumab) 300mg"
			if (fnBiologicsAdminStripNonNumerics(OBSNOW("DUPIXENT DOS"))=="600") then
				units="2"
			endif
		case DOCUMENT.SELECT_DRUG=="ILARIS" and match(OBSANY("ILARIS BNB"),"Yes")>0
			description="Injection, canakinumab (Ilaris), 1 mg"
			if (fnBiologicsAdminStripNonNumerics(OBSNOW("ILARIS DOSE"))=="300") then
				units="2"
			endif
		case DOCUMENT.SELECT_DRUG=="NUCALA" and match(OBSANY("NUCALA BNB"),"Yes")>0
			units="100"
			description="Injection, mepolizumab (Nucala), 1 mg"
	endcond
	if (description<>"") then
		/* Check to see if the order has already been entered today */
		okToUse="Yes"
		if (match(ORDERS_NEW(),description)>0) then
			okToUse=useryesno("Order '"+description+"' is already in today's orders, do you want to add it anyway?")
		endif
		if (okToUse=="Yes") then
			/* Create the order */
			local comment=""
			if match(description,"Xolair")>0 then
				comment="Verify # of units "+units
			endif
			retcode = MEL_ADD_ORDER("S","Drugs/Injectables/Infusion",description,"",dxCode,dxDescr,comment,units,"",provider,"")
			if (retcode<>"0") then
				userok("Error calling MEL_ADD_ORDER. Error code "+fnBiologicsAdminDecodeError(retcode))
			endif
			/* Special processing if using Xolair pre-filled syringes and used both 75 and 150 syringes */
			if description2<>"" then
				comment="Verify # of units "+units2
				retcode = MEL_ADD_ORDER("S","Drugs/Injectables/Infusion",description2,"",dxCode,dxDescr,comment,units2,"",provider,"")
				if (retcode<>"0") then
					userok("Error calling MEL_ADD_ORDER. Error code "+fnBiologicsAdminDecodeError(retcode))
				endif
			endif
		endif
	endif
}
/*********************************************************/
/* Function: fnBiologicsAdminStripNonNumerics(theString)
/* Purpose:  Do a left-to-right character scan of the passed-in value until
/*			  we hit a non-numeric character, then return the scanned chars.
/*********************************************************/
fn fnBiologicsAdminStripNonNumerics(theString)
{
	local i,ch,retStr=""
	for i=1,i<=size(theString),i=i+1
	do
		ch=sub(theString,i,1)
		if (ch="0" or ch="1" or ch="2" or ch="3" or ch="4" or ch="5" or ch="6" or ch="0" or ch="7" or ch="8" or ch="9" or ch="." or ch="," or ch=":") then
			retStr=retStr+ch
		else
			break
		endif
	endfor
	return retStr
}
fn fnBiologicsAdminDecodeError(retcode) 
{
	cond
	case retcode=="-1" return "-1 : Invalid Order Type"
	case retcode=="-2" return "-2 : Invalid order category"
	case retcode=="-3" return "-3 : Invalid Description"
	case retcode=="-4" return "-4 : Order obsolete"
	case retcode=="-5" return "-5 : Invalid Diagnosis Code"
	case retcode=="-6" return "-6 : Comments field too long"
	case retcode=="-7" return "-7 : Invalid Priority"
	case retcode=="-8" return "-8 : Invalid or obsolete authorizing provider"
	case retcode=="-9" return "-9 : Invalid order date"
	case retcode=="-10" return "-10: Invalid Modifier"
	case retcode=="-11" return "-11: Additional information is required for this order"
	case retcode=="-12" return "-12: Unequal number of diagnosis codes and diagnosis code descriptions"
	case retcode=="-13" return "-13: Invalid quantity or units"
	else
		return ""
	endcond
}

!fn fnBiologicsAdminProblemList(strList)
{
   local lCounter
   local lProbArray
   local lStart
   local lBuffer
   local lFormattedList = ""

   /* Create array of medications */
   lProbArray = getfield(strList, "|", "")

   for lCounter = 1, lCounter <= size(lProbArray), lCounter = lCounter + 1 do
      /* Create array for each medication */
      lProbArray[lCounter] = getfield(lProbArray[lCounter], "^", "")

      /* Need to remove commas from problem name */
      lBuffer = ReplaceStr(lProbArray[lCounter][2], ",", ";")
      /* Append problem and ICD-9 code to formatted list */
      lFormattedList = lFormattedList + lBuffer + "^" + lProbArray[lCounter][8] + ","
   endfor

   /* Remove trailing  comma */
	if (lCounter > 1) then
		lFormattedList = remove(lFormattedList, size(lFormattedList))
	endif

      /* Return comma separated list */
   return (lFormattedList)
}

fn fnBiologicsAdminGetProblemDescriptions()
{
	local lCounter
	local lSize
	local lResult = str("")
	local lProblemList = getfield(DOCUMENT.PROBLEMS, ",", "")	/* Get selected problems and separate into array by commas */

	lSize = size(lProblemList)	/* Determine size of array */

   for lCounter = 1, lCounter <= lSize, lCounter = lCounter + 1 do	/* Loop through all rows of the array */
   	lProblemList[lCounter] = getfield(lProblemList[lCounter], "^", "")	/* Separate problem description and code */
   	lResult = lResult + lProblemList[lCounter][1] + "|"	/* Join descriptions into a string separated by pipes */
   endfor

	if (lResult <> "") then
		lResult = remove(lResult, size(lResult))	/* Remove trailing pipe (|) */
	endif
}

fn fnBiologicsAdminGetProblemCodes()
{
	local lCounter
	local lSize
	local lResult = str("")
	local lProblemList = getfield(DOCUMENT.PROBLEMS, ",", "")	/* Get selected problems and separate into array by commas */

	lSize = size(lProblemList)	/* Determine size of array */

   for lCounter = 1, lCounter <= lSize, lCounter = lCounter + 1 do	/* Loop through all rows of the array */
   	lProblemList[lCounter] = getfield(lProblemList[lCounter], "^", "")	/* Separate problem description and code */
   	lResult = lResult + lProblemList[lCounter][2] + "|"	/* Join codes into a string separated by pipes */
   endfor

	if (lResult <> "") then
		lResult = remove(lResult, size(lResult))	/* Remove trailing pipe (|) */
	endif
}



/*******************************************************************/
/* Function: fnCalculateElapsedTime
/* Purpose: Given a start time and finish time, calculate interval.
/*******************************************************************/
fn fnCalculateElapsedTime(strStart, strFinish)
{
local lStart
local lFinish
local lTime
local temp=""
/* remove all non-numeric chars from time (except ":") */
local i
for i=1,i<=size(strStart),i=i+1
do
	if ((strStart[i]>="0" and strStart[i]<="9")or strStart[i]=":") then
		temp=temp+strStart[i]
	endif
endfor
strStart=temp
temp=""
for i=1,i<=size(strFinish),i=i+1
do
	if ((strFinish[i]>="0" and strFinish[i]<="9")or strFinish[i]=":") then
		temp=temp+strFinish[i]
	endif
endfor
strFinish=temp
/* Convert times into arrays with hours and minutes */
lStart = getfield(strStart, ":")
lFinish = getfield(strFinish, ":")

/* Convert strings to values */
lStart[1] = val(lStart[1])
lStart[2] = val(lStart[2])
lFinish[1] = val(lFinish[1])
lFinish[2] = val(lFinish[2])

/* if finish hour < starting hour, must have crossed 12:00 noon, so add 12 to finish hour */
if (lFinish[1]<lStart[1]) then
	lFinish[1]=lFinish[1]+12
endif

/* If minutes for finish time less than start time add 60 minute and subtract 1 hour to facilitate calculation */
if (lFinish[2] < lStart[2]) then
    lFinish[1] = lFinish[1] - 1
    lFinish[2] = lFinish[2] + 60
endif

/* Subtract hours and minutes and create string */
temp=str(lFinish[2] - lStart[2])
if (size(temp)==1) then
	temp="0"+temp
endif
lTime = str(lFinish[1] - lStart[1]) + ":" + temp
return (lTime)
}

/*****************************************************/
/* Function: fnBiologicsAdminGetMostRecentIndication()
/* Purpose:  Returns value of most recently recorded biologic indication, if any
/*****************************************************/
!fn fnBiologicsAdminGetMostRecentIndication()
{
	local i,mostRecentDate="",indication=""
	for i=1,i<=getnargs(),i=i+1 do
		if LAST_SIGNED_OBS_VALUE(getarg(i))<>"" then
			if mostRecentDate=="" or DURATIONDAYS(mostRecentDate,LAST_SIGNED_OBS_DATE(getarg(i)))>0 then
				mostRecentDate=LAST_SIGNED_OBS_DATE(getarg(i))
				indication=LAST_SIGNED_OBS_VALUE(getarg(i))
			endif
		endif
	endfor
	return indication 
}
/*****************************************************/
/* Function: fnBiologicsAdministrationGetLastOfficeVisit()
/* Purpose:  Called from initialization above to
/*			  find the most recent appointment and,
/*			  if > 6 months ago, create message.
/****************************************************/
!fn fnBiologicsAdministrationGetLastOfficeVisit()
{
	local msg=""
	DOCUMENT.RED_OR_YELLOW="yellow"
	local numRows=getrowcount("_PatientAppointment") 
	local i,id,apptType,apptDate,apptStatus,apptComments,book,name,futureApptDate=""
	//Find most recent indication, if any
	local indication=fnBiologicsAdminGetMostRecentIndication("XOLAIR INDIC","CINQAIR IND","DUPIXENT IND","FASENRA IND","NUCALA INDIC")
	//Find most recent patient office visit:
	local monthsOrYears="6 months"
	if (numRows>0) then
		for i=(numRows-1),i>=0,i=i-1
		do	
			id=getrow("_PatientAppointment",i,"APPTTYPE")
			apptType=find("_MELApptDef","APPTTYPE","ID",id)
			apptComments=getrow("_PatientAppointment",i,"COMMENTS")
			book=getrow("_PatientAppointment",i,"BOOKLIST")
			name=find("_MELBook","NAME","ID",ReplaceStr(book,":",""))
			if (match(apptType,"Est Patient")>0 or 
				match(apptType,"Est Pt Visit")>0 or 
				match(apptType,"New Pt Visit")>0 or 
				match(apptType,"New Patient")>0 or
				match(name," MD ")>0 or
				match(name," NP ")>0 or
				match(name," FNP")>0 or
				match(name," PA-C ")>0) then
				apptStatus=getrow("_PatientAppointment",i,"APPTSTATUS")
				apptDate=getrow("_PatientAppointment",i,"APPTDATE")
				if (apptStatus==0 and DURATIONDAYS(apptDate,str(._TODAYSDATE))<=0) then
					/* found a future appointment, so save the date */
					futureApptDate=apptDate
				endif
				if (apptStatus==3  or match(apptComments,"ARRIVED")>0) then
					/* found a previous MD or PA appointment */
					if indication<>"" then
						//go through indication logic
						if  DURATIONDAYS(apptDate,str(DOCUMENT.CLINICALDATE))>180 then
							DOCUMENT.RED_OR_YELLOW="red"
							msg="A follow up appointment is suggested."
						else
						cond
								case indication=="asthma" or indication=="EGPA" or indication=="HES" or indication=="chronic rhinosinusitis with polyps" or match(indication,"urticaria")>0:
									if DURATIONDAYS(apptDate,str(DOCUMENT.CLINICALDATE))>90  then
										msg="If patient is new to therapy or not well controlled, a follow up appointment is suggested."
									endif
								case indication=="atopic dermatitis":
									if DURATIONDAYS(apptDate,str(DOCUMENT.CLINICALDATE))>90  then
										msg="If patient is new to therapy, is not well controlled or has severe disease, a follow up appointment is suggested."
									endif
							endcond
						endif
						break 
					else
						//else just use old logic:
						if (DURATIONDAYS(apptDate,str(._TODAYSDATE))>180) then
							/* visit was > 6 months, so set message and break */
							msg="Last doctor visit was more than "+monthsOrYears+" ago (appointment type '"+apptType+"' on "+apptDate+")"
							if (futureApptDate<>"") then
								msg=msg+". Next doctor visit: "+futureApptDate+"."
							endif
							break
						else
							/* visit was <= 6 months ago, so just break */
							break
						endif
					endif
				endif
			endif
		endfor
	endif
	return msg
}
/*****************************************************/
/* Function: fnBiologicsAdministrationGetLastOfficeVisit()
/* Purpose:  Called from initialization above to
/*			  find the most recent office visit and,
/*			  if > 6 months ago, create message.
/****************************************************
!fn fnBiologicsAdministrationGetLastOfficeVisit()
{
	local msg=""
	local numRows=getrowcount("_PrintDocs") 
	local i,id,docType,visitDate
	local numDaysToCount=180,futureApptDate=""
	local monthsOrYears="6 months"
	if (numRows>0) then
		for i=(numRows-1),i>=0,i=i-1
		do	
			docType=getrow("_PrintDocs",i,"DocType")
			if (docType==1) then
				/* Get clinical date *
				visitDate=getrow("_PrintDocs",i,"DocDate")
				if visitDate<>str(._TODAYSDATE) then
					if (DURATIONDAYS(visitDate,str(._TODAYSDATE))>numDaysToCount) then
						/* visit was > 6 months ago, so set message and break *
						msg="Last doctor visit was more than "+monthsOrYears+" ago (on "+visitDate+")"
						if (appt_next()<>"<None>") then
							msg=msg+". Next doctor visit: "+appt_next()+"."
						endif
						break
					else
						/* visit was <= 6 months ago, so just break * 
						break
					endif
				endif
			endif
		endfor
	endif
	return msg
}
/*******************************************************/
/* Function: fnBiologicsAdministrationClearAlert()
/* Purpose: Clear alert message
/*******************************************************/
fn fnBiologicsAdministrationClearAlert()
{	
	if (useryesno("Clear alert message?")=="Yes") then
		DOCUMENT.MSG_ALERT=""
	endif
}
/***************************************************************************/
/* Watcher to set document variable that sets ACT action item RED
/***************************************************************************/
{!DOCUMENT.ACT_RED_MSG=fnImmunotherapySetACTRedMessage(LASTOBSDATE("ASTHSEV1"))}
!fn fnImmunotherapySetACTRedMessage(actDate)
{
	if (actDate=="") then
		return "No ACT scores found. ACT should be done every 6 months."
	endif
	if (DURATIONDAYS(actDate,str(._TODAYSDATE))>=182) then
		return "Asthma Control Testing should be done every 6 months. Last ACT was on "+actDate
	endif
	return ""
}
